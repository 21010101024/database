class CityModel{

  late int _CityID;
  late String _CityName;
  late int _StateID;

  int get CityID => _CityID;

  set CityID(int CityID) {
    _CityID = CityID;
  }

  String get CityName => _CityName;

  set CityName(String CityName) {
    _CityName = CityName;
  }

  int get StateID => _StateID;

  set StateID(int StateID) {
    _StateID = StateID;
  }

}
class UserModel{

  late int _UserID;
  late String _UserName;
  late String _DOB;
  late int _CityID;

  int get UserID => _UserID;

  set UserID(int UserID) {
    _UserID = UserID;
  }

  String get UserName => _UserName;

  set UserName(String UserName) {
    _UserName = UserName;
  }

  String get DOB => _DOB;

  set DOB(String DOB) {
    _DOB = DOB;
  }

  int get CityID => _CityID;

  set CityID(int CityID) {
    _CityID = CityID;
  }

}
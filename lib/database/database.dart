import 'dart:io';
import 'package:database/model/city_model.dart';
import 'package:database/model/user_model.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'demo.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<void> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "demo.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data = await rootBundle.load(join('assets/database', 'demo.db'));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
    }
  }

  Future<void> upsertIntoUserTable({cityid, userName, dob, userID}) async {
    Database db = await initDatabase();
    Map<String, Object?> map = Map();
    map['UserName'] = userName;
    map['DOB'] = dob;
    map['CityID'] = cityid;
    if (userID != -1) {
      map['UserID'] = userID;
      await db.update("Tb_User", map, where: 'UserID = ?', whereArgs: [userID]);
    } else {
      await db.insert("Tb_User", map);
    }
  }

  Future<List<CityModel>> getCityListFromTbl() async {
    List<CityModel> cityList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery("select * from Mst_City");
    CityModel model = CityModel();
    model.CityID = -1;
    model.CityName = "Select City";
    model.StateID = -1;
    cityList.add(model);
    for (int i = 0; i < data.length; i++) {
      model = CityModel();
      model.CityID = data[i]['CityID'] as int;
      model.CityName = data[i]['CityName'].toString();
      model.StateID = data[i]['StateID'] as int;
      cityList.add(model);
    }
    print("User : ${data.length}");
    return cityList;
  }

  Future<List<UserModel>> getUserListFromTbl() async {
    List<UserModel> userList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery("select * from Tb_User");
    for (int i = 0; i < data.length; i++) {
      UserModel model = UserModel();
      model.CityID = data[i]['CityID'] as int;
      model.UserID = data[i]['UserID'] as int;
      model.UserName = data[i]['UserName'].toString();
      model.DOB = data[i]['DOB'].toString();
      userList.add(model);
    }
    print("User : ${data.length}");
    return userList;
  }

  Future<int> deleteUserFromUserTable(UserID) async {
    Database db = await initDatabase();
    int deletedId = await db.delete(
      "Tb_User",
      where: "UserID = ?",
      whereArgs: [UserID],
    );
    return deletedId;
  }
}

import 'package:intl/intl.dart';

class Utils{

  static String getFormatedDateTine(dateToFormate) {
    if (dateToFormate != null) {
      return DateFormat('dd-mm-yyyy').format(dateToFormate).toString();
    } else {
      return DateFormat('dd-mm-yyyy').format(DateTime.now()).toString();
    }
  }

}
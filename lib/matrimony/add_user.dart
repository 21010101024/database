import 'dart:math';

import 'package:database/database/database.dart';
import 'package:database/model/city_model.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:intl/intl.dart';

import '../model/user_model.dart';

class AddUserPage extends StatefulWidget {
  late UserModel? model;

  AddUserPage({Key? key, this.model}) : super(key: key);

  @override
  State<AddUserPage> createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  late CityModel model;
  bool isGetCity = true;
  DateTime selectedDate = DateTime.now();
  late TextEditingController nameController;
  final formKey = GlobalKey<FormState>();

  MyDatabase myDatabase = MyDatabase();

  @override
  void initState() {
    nameController = TextEditingController(
        text: widget.model != null ? widget.model!.UserName.toString() : '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(15, 15, 15, 0.9),
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Add User"),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 350,
            height: 350,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(top: 10),
            decoration: BoxDecoration(
              color: Color.fromRGBO(10, 10, 10, 0.9),
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.2), blurRadius: 10),
              ],
            ),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Container(
                    width: 310,
                    height: 50,
                    margin: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: nameController,
                      style: TextStyle(color: Colors.white70),
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(color: Colors.white54),
                        ),
                        hintText: "Username",
                        hintStyle: TextStyle(color: Colors.white54),
                      ),
                      validator: (value) {
                        if (value == null || value.trim!().length == 0) {
                          return "Enter Valid Name";
                        }
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    width: double.infinity,
                    child: FutureBuilder<List<CityModel>>(
                      builder: (context, snapshot) {
                        if (snapshot.hasData && snapshot.data != null) {
                          if (isGetCity) {
                            model = snapshot.data![0];
                          }
                          return DropdownButtonHideUnderline(
                            child: DropdownButton2(
                              // hint: Row(
                              //   children: const [
                              //     Icon(
                              //       Icons.list,
                              //       size: 16,
                              //       color: Colors.yellow,
                              //     ),
                              //     SizedBox(
                              //       width: 4,
                              //     ),
                              //     Expanded(
                              //       child: Text(
                              //         'Select Item',
                              //         style: TextStyle(
                              //           fontSize: 14,
                              //           fontWeight: FontWeight.bold,
                              //           color: Colors.yellow,
                              //         ),
                              //         overflow: TextOverflow.ellipsis,
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              items: snapshot.data!
                                  .map((item) => DropdownMenuItem<CityModel>(
                                        value: item,
                                        child: Text(
                                          item.CityName.toString(),
                                          style: const TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ))
                                  .toList(),
                              value: model,
                              onChanged: (value) {
                                setState(() {
                                  isGetCity = false;
                                  model = value!;
                                });
                              },
                              icon: const Icon(
                                Icons.arrow_forward_ios_outlined,
                                size: 10,
                              ),
                              buttonDecoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(color: Colors.grey)),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                      future:
                          isGetCity ? myDatabase.getCityListFromTbl() : null,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      pickDateDialog();
                    },
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        getFormatedDateTine(selectedDate),
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  InkWell(
                    onTap: () async {
                      if (formKey.currentState!.validate()) {
                        if (model.CityID == -1) {
                          showAlertDialog(context);
                        } else {
                          await myDatabase.upsertIntoUserTable(
                            cityid: model.CityID,
                            dob: selectedDate.toString(),
                            userName: nameController.text,
                            userID: widget.model != null
                                ? widget.model!.UserID
                                : -1,
                          );
                          print(
                              "Data To Store In DB : ${model.CityID.toString()} ${model.CityName.toString()} ${selectedDate.toString()}");
                          Navigator.of(context).pop(true);
                        }
                      }
                    },
                    child: Container(
                      width: 130,
                      height: 50,
                      decoration: BoxDecoration(
                        color: Colors.blueAccent,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w800),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Please Select The City"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void pickDateDialog() {
    showDatePicker(
            context: context,
            initialDate: selectedDate,
            firstDate: DateTime(1950),
            //DateTime.now() - not to allow to choose before today.
            lastDate: DateTime.now())
        .then(
      (pickedDate) {
        if (pickedDate == null) {
          return;
        }
        setState(() {
          selectedDate = pickedDate;
        });
      },
    );
  }

  String getFormatedDateTine(dateToFormate) {
    if (dateToFormate != null) {
      return DateFormat('dd-mm-yyyy').format(dateToFormate).toString();
    } else {
      return DateFormat('dd-mm-yyyy').format(DateTime.now()).toString();
    }
  }
}

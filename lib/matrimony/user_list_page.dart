import 'package:database/database/database.dart';
import 'package:database/matrimony/add_user.dart';
import 'package:database/model/city_model.dart';
import 'package:database/model/user_model.dart';
import 'package:flutter/material.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  // late Future<List<Map<String, Object?>>> userListFuture;

  List<UserModel> localList = [];
  List<UserModel> searchList = [];
  bool isGetData = true;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    // controller.addListener(() {
    //   print('Controller : ${controller.text}');
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(15, 15, 15, 0.9),
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text("User List"),
          actions: [
            InkWell(
              onTap: () {
                var temp = Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) {
                      return AddUserPage(model: null);
                    },
                  ),
                ).then(
                  (value) {
                    print(':::::$value');
                    if(value == true){
                      setState(() {
                        localList.clear();
                        searchList.clear();
                        isGetData = true;
                      });
                    }

                  },
                );
              },
              child: Container(
                margin: EdgeInsets.only(right: 10),
                child: Icon(Icons.add),
              ),
            )
          ],
        ),
        body: FutureBuilder<List<UserModel>>(
          builder: (context, snapshot) {
            if (snapshot != null && snapshot.hasData) {
              if (isGetData) {
                localList.addAll(snapshot.data!);
                searchList.addAll(localList);
              }
              isGetData = false;
              return Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        top: 30, right: 10, bottom: 10, left: 10),
                    width: 400,
                    child: TextField(
                      controller: controller,
                      onChanged: (value) {
                        print('${value}');
                        searchList.clear();
                        for (int i = 0; i < localList.length; i++) {
                          if (localList[i].UserName.toLowerCase().contains(
                                value.toLowerCase(),
                              )) {
                            searchList.add(localList[i]);
                          }
                        }
                        setState(() {});
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(width: 3, color: Colors.amber),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide(color: Colors.white54),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide(
                              color: Colors.white,
                            )),
                        hintText: "Search",
                        hintStyle: TextStyle(color: Colors.grey),
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: searchList!.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) {
                                  return AddUserPage(
                                    model: searchList![index],
                                  );
                                },
                              ),
                            ).then(
                              (value) {
                                setState(() {});
                              },
                            );
                          },
                          child: Container(
                            margin:
                                EdgeInsets.only(bottom: 8, left: 10, right: 10),
                            child: ListTile(
                              title: Text(
                                searchList![index].UserName.toString(),
                                style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 0.8),
                                ),
                              ),
                              subtitle: Text(
                                searchList![index].DOB.toString(),
                                style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 0.5),
                                ),
                              ),
                              leading: CircleAvatar(
                                backgroundColor: Colors.black26,
                                child:
                                    Text(searchList![index].UserID.toString()),
                              ),
                              // tileColor: Color.fromRGBO(230, 245, 240, 0.9),
                              tileColor: Color.fromRGBO(30, 30, 30, 0.9),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              trailing: Container(
                                width: 50,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        showAlertDialog(context, index);
                                      },
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.red,
                                      ),
                                    ),
                                    Icon(
                                      Icons.keyboard_arrow_right,
                                      color: Color.fromRGBO(255, 255, 255, 0.4),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              );
            } else {
              return Center(
                // child: Text('No User'),
                child: CircularProgressIndicator(),
              );
            }
          },
          future: isGetData ? MyDatabase().getUserListFromTbl() : null,
        ));
  }

  showAlertDialog(BuildContext context, index) {
    Widget yesButton = TextButton(
      child: Text(
        "Yes",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.8),
        ),
      ),
      onPressed: () async {
        int deletedUserID = await MyDatabase()
            .deleteUserFromUserTable(searchList[index].UserID);
        if (deletedUserID > 0) {
          searchList.removeAt(index);
        }
        Navigator.pop(context);
        setState(() {});
      },
    );
    Widget noButton = TextButton(
      child: Text(
        "No",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.8),
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(
        "Alert",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.8),
        ),
      ),
      content: Text(
        "Are you sure want to delete?",
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.5),
        ),
      ),
      backgroundColor: Color.fromRGBO(80, 80, 80, 0.5),
      actions: [
        yesButton,
        noButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
